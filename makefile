
CXX=g++
DEBUG=yes
STATIC=yes

CIBLE=unit_conversion
EXE=$(CIBLE)
EXEC=$(EXE)
SRC_DIR=src
OBJ_DIR=build
INC_DIR=include

SRC=$(wildcard $(SRC_DIR)/*.cpp) 
INC=$(wildcard $(INC_DIR)/*.hpp) 
OBJ=$(SRC:$(SRC_DIR)/%.cpp=$(OBJ_DIR)/%.o)
CXXFLAGS= -W -Wall -I$(INC_DIR)

ifeq ($(DEBUG), yes)
  CXXFLAGS+= -g
else 
  CXXFLAGS+= -O2
endif

all:$(EXEC)
ifeq ($(DEBUG), yes)
	@echo Generation en mode debug
else
	@echo Generation en mode release
endif


build/%.o: $(SRC_DIR)/%.cpp $(INC)
	@echo Compilation C++ $< to $@
	$(CXX)  -c $< -o $@ $(CXXFLAGS)

$(EXEC):$(OBJ) $(OBJ_CIBLE)
	@echo -----LINKAGE-----
	$(CXX) -o $@ $(OBJ) $(OBJ_CIBLE) $(LDFLAGS)

.PHONY : clean run

run:
	$(EXEC)

clean:
	rm -rf $(OBJ_DIR)/*.o *.exe *~ */*~ */*/*~ $(CIBLE)
